var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

var SeleccionarFecha = function(){
	var _fecha = new Date();
	var factor = 0;
	switch(_fecha.getDay())
	{
		case 0:
			factor = 1;
			break;
		case 6:
			factor = 2;
			break;
		case 5:
			factor = 3;
			break;
		default: 
			factor = 1;
	}
	if(_fecha.getHours() > 16 ){
		_fecha = (_fecha.getDate()+factor)+ '/' + monthNames[_fecha.getMonth()] + '/' + _fecha.getFullYear().toString().substr(2,2);
	}
	else{ //Antes de las 4 de la tarde
		if(_fecha.getDay() >= 1 && _fecha.getDay() <=5)
			_fecha = _fecha.getDate() + '/' + monthNames[_fecha.getMonth()] + '/' + _fecha.getFullYear().toString().substr(2,2);	
		else
			_fecha = (_fecha.getDate()+ factor) + '/' + monthNames[_fecha.getMonth()] + '/' + _fecha.getFullYear().toString().substr(2,2);
			
	}
	jQuery("#duedate").val(_fecha);
}

var Componentes = function(select)
{
	jQuery.each(select, function(index, val) {
		 var element = document.getElementById("components-textarea");
		document.getElementById("duedate").focus();
		element.focus();
		element.value = val;
		document.getElementById("environment").focus();
	});
}


var Asignacion = function(valor)
{
	jQuery("#assignee").val(valor);
}

var selectOperacion = function(operacion,opcion)
{
	var _operacion = jQuery('#customfield_10100 option').eq(operacion).attr('value');
  	jQuery("#customfield_10100").val(_operacion);
  	jQuery("#customfield_10100").change();
  	var _opcion = jQuery('#customfield_10100\\:1 option').eq(opcion).attr('value');
  	jQuery("#customfield_10100\\:1").val(_opcion);
}
var selectInjectedInOperation = function(operacion,opcion)
{
	var _operacion = jQuery('#customfield_10102 option').eq(operacion).attr('value');
  	jQuery("#customfield_10102").val(_operacion);
  	jQuery("#customfield_10102").change();
  	var _opcion = jQuery('#customfield_10102\\:1 option').eq(opcion).attr('value');
  	jQuery("#customfield_10102\\:1").val(_opcion);
}
var detectedInOperation = function(operacion,opcion){
	var _operacion = jQuery('#customfield_10103 option').eq(operacion).attr('value');
  	jQuery("#customfield_10103").val(_operacion);
  	jQuery("#customfield_10103").change();
  	var _opcion = jQuery('#customfield_10103\\:1 option').eq(opcion).attr('value');
  	jQuery("#customfield_10103\\:1").val(_opcion);
}
var selectCobrable = function(value){
	jQuery("#customfield_10330").val(value);
}